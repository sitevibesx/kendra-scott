SiteVibes.createBannerScaffoldingHTML = function(){
    var appendHTML = 
    '<div class="sv-banner-title">Trending Products</div>\
        <div id="sv_arrow_left_wrap" class="grid-5">\
                <i id="leftarrow" onclick="SiteVibes.bannerLeft();" class="fa fa-chevron-circle-left fa-2x"></i>\
        </div>\
        <div id="sv-products">\
            <div id="sv-products-list"></div>\
        </div>\
        <div id="sv_arrow_right_wrap" class="grid-5">\
            <i id="rightarrow" onclick="SiteVibes.bannerRight();" class="fa fa-chevron-circle-right fa-2x"></i>\
        </div>\
    </div>';
    return appendHTML;
};

SiteVibes.createBannerResultHTML = function(items, dir){
    var appendHTML = '<div class="sv-banner-group-wrap" '+(typeof(dir) == 'string' ? 'style="left:'+dir+'100%"' : '')+'>';
    
    var shareHTML = '';
    if(this.injectSocialButtons){
        var shareHTML = '<div class="sv_share_wrap">';

        for(i=0;i<this.socialShareButtons.length;i++){
                if(typeof(this[this.socialShareButtons[i]+'Share']) == 'function'){
                    shareHTML += '<div onclick="SiteVibes.'+this.socialShareButtons[i]+'Share(this);return false;" \
                          class="sv_share sv_'+this.socialShareButtons[i]+'"></div>';
                }
        }
        shareHTML += '</div>';
    }


    for(i=0;i<items.length;i++){
        //price / sale price formatting stuff
        var prices = decodeURIComponent(items[i].price).split(',');
        var was = prices
        var sale = 0;
        var save = 0;
        if(prices.length > 1 && prices[1]){
            sale = prices[0];
            was  = prices[1];
            var fWas = was.replace(/Was\s\$/, '');
            var fSale = sale.replace(/\$/, '');
            save = (parseFloat(fWas) - parseFloat(fSale)).toFixed(2);
        }

        //url = url + urlbuilder +"utm_source=sv-site&utm_medium=banner&utm_campaign=sitevibes";
        var tTypeObj = SiteVibes.determineTrendingType(items[i]);

        //temp fix to sanitize image links
        var img = '';
        if(items[i].image){
            img = items[i].image.replace('"', '');
        }
        
        appendHTML += 
        '<div class="sv-product-card grid-30 tablet-grid-50 mobile-grid-100 grid-parent">\
            <div class="sv_banner_inner_wrap">\
                <i style="color:'+tTypeObj.color+'!important;" class="'+tTypeObj.iconClass+'"></i>'+
                shareHTML+
                '<p class="sv-trending-type">' + tTypeObj.tType + '</p>\
                <div style="text-align:center;" class="sv-product-details grid-100">\
                    <div class="sv-product-image-container grid-100"><a href="' + items[i].url + '"><img src="' + img + '" class="sv-item-image" /></a></div>\
                    <p class="sv-product-name grid-100"><a href="' + items[i].url + '">' + decodeURIComponent(escape(items[i].name)) + '</a></p>\
                    <span class="sv-item-price '+(sale?'sv-sale':'')+'">'+(sale ? sale : was)+'</span>\
                    '+(sale ?
                    '<span class="sv-item-price-meta">\
                        <span class="sv-item-price-meta-was">'+was+'</span>'+
//                        <span style="font-weight:bold;font-size:12px" class="item-price-meta-save">Save $'+save+'</span>
                    '</span>' : '')+'\
                    <a class="view-details" href="'+items[i].url+'">View Details</a>'+
               '</div>\
           </div>\
       </div>';
    }

    //add any blank ones in case we didnt' get enough results
    var toAppend = this.bannerPerPage-items.length;
    for(i=0;i<toAppend;i++){
                appendHTML += 
        '<div class="sv-product-card grid-30 tablet-grid-50 mobile-grid-100 grid-parent">\
            <p class="sv-trending-type"></p>\
            <div class="sv-product-details grid-100">\
                <div class="sv-product-image-container grid-100"></div>\
                <p class="sv-product-name grid-100"></p>\
            </div>\
        </div>';
    }
               
    appendHTML += '</div>';
    return appendHTML;
}

SiteVibes.prepareTWPing = function(node){
        var session = this.uuid();
        var today = new Date();
        var fdate = today.getUTCFullYear()+"-"+
                   (today.getUTCMonth()+1)+"-"+
                    today.getUTCDate()+" "+today.getUTCHours()+":"+
                    today.getUTCMinutes()+":"+today.getUTCSeconds();
        this.q.session = session;
        this.q.time = fdate;

        var media = node.parentNode.parentNode.querySelector('.sv-item-image');
        media = (media && media.src ? media.src : '');
        this.q.image = media;

        var text  = node.parentNode.parentNode.querySelector('.sv-product-name > a');
        text = (text && text.textContent ? text.textContent : '');
        this.q.name = text;

        this.q.url             = node.parentNode.parentNode.querySelector('a').href;
        this.q.host            = encodeURIComponent(location.host);

        this.q.productCategory = this.productCategory;
        this.q.pagetype        = 'banner';
        this.q.id              = encodeURIComponent(this.tmiID);
    
        this.q.price = encodeURIComponent(node.parentNode.parentNode.querySelector('.sv-item-price').textContent.trim());


        this.ping();
};